# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'remove database'
ActiveRecord::Base.connection.execute("TRUNCATE serial_data")
ActiveRecord::Base.connection.execute("TRUNCATE members")
ActiveRecord::Base.connection.execute("TRUNCATE balances")
ActiveRecord::Base.connection.execute("TRUNCATE bonus")
puts 'create bonus'
Bonus.create_seed_all_bonus
#puts 'create users'
#User.create(email: 'test@bbest.com', password: 'test@bbest.com', password_confirmation: 'test@bbest.com')
puts 'create serial'
SerialData.generate_serial_data_seeds(1000) 
puts 'create members'
Member.create_users_seed #== 100 kelipatan 10
puts 'done'

#remove all data
['pages','programs','faqs','galleries','articles','agendas'].each do |x|
  ActiveRecord::Base.connection.execute("TRUNCATE #{x}")
end

Page.generate_seeds
Program.generate_seeds

#file = File.open(Rails.root.to_s + '/public/x.jpg')
#5.times do |i|
#  title = Faker::Name.title
#  content = Faker::Lorem.paragraph(10)
#  Faq.create(title: title, description: content)
#  Gallery.create(title: title, image: file)
#  Article.create(title: title, description: content, image: file)
#  Agenda.create(title: title, description: content)
#end

