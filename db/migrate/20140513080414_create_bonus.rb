class CreateBonus < ActiveRecord::Migration
  def change
    create_table :bonus do |t|
      t.integer :total_left
      t.integer :total_right
      t.string :bonus_type
      t.string :name

      t.timestamps
    end
  end
end
