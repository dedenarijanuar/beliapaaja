class AddColumnTableNumberToMembers < ActiveRecord::Migration
  def change
    add_column :members, :table_number, :integer
  end
end
