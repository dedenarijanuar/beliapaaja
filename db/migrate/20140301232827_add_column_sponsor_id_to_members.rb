class AddColumnSponsorIdToMembers < ActiveRecord::Migration
  def change
    add_column :members, :sponsor_id, :integer
  end
end
