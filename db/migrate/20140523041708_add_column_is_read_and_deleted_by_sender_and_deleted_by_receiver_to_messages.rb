class AddColumnIsReadAndDeletedBySenderAndDeletedByReceiverToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :is_read, :boolean
    add_column :messages, :deleted_by_sender, :boolean
    add_column :messages, :deleted_by_receiver, :boolean
  end
end
