class CreateSerialData < ActiveRecord::Migration
  def change
    create_table :serial_data do |t|
      t.string :serial
      t.string :pin
      t.boolean :status

      t.timestamps
    end
  end
end
