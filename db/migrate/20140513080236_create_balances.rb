class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.date :get_at
      t.string :balance_type
      t.integer :amount
      t.integer :member_id

      t.timestamps
    end
  end
end
