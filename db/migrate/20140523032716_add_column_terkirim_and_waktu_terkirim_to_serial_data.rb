class AddColumnTerkirimAndWaktuTerkirimToSerialData < ActiveRecord::Migration
  def change
    add_column :serial_data, :terkirim, :boolean
    add_column :serial_data, :waktu_terkirim, :datetime
  end
end
