class CreateMemberBonus < ActiveRecord::Migration
  def change
    create_table :member_bonus do |t|
      t.date :get_at
      t.integer :status
      t.integer :member_id

      t.timestamps
    end
  end
end
