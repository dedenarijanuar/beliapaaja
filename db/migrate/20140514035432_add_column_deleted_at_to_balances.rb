class AddColumnDeletedAtToBalances < ActiveRecord::Migration
  def change
    add_column :balances, :deleted_at, :datetime
  end
end
