class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.date :get_at
      t.integer :member_id
      t.integer :earned
      t.integer :used
      t.string :transaction_type
      t.integer :sender_id
      t.text :description

      t.timestamps
    end
  end
end
