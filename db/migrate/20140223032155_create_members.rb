class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :nick_name
      t.string  :nama
      t.integer :serial_data_id
      t.date  :tanggal_lahir
      t.string  :tempat_lahir
      t.string  :no_identitas
      t.boolean :jenis_kelamin
      t.string  :kewarganegaraan
      t.string  :status_sipil
      t.string  :alamat
      t.string  :kota
      t.string  :negara
      t.string  :telepon
      t.string  :agama
      t.string  :ahli_waris
      t.string  :ibu_kandung
      t.string  :provinsi
      t.integer :kode_pos
      t.string  :handphone
      t.string  :no_npwp
      t.string  :hubungan
      t.string  :nama_bank
      t.string  :nama_pemilik
      t.string  :no_rekening
      t.string  :email
      t.boolean :is_active, default: true
      t.timestamps
    end
  end
end
