module ApplicationHelper

  def profile_menu
    [['Profile Saya', my_profile_path], ['Edit Profil',edit_profile_path] ,['Ganti Password', change_pin_path]] #, ['Ganti PIN Saldo', change_pin_path]
  end
  
  def network_menu
    [['Mitra Saya', diagram_network_path], ['Pertumbuhan Mitra', history_network_path], ['Afiliasiku', afiliasiku_path]]
  end
  
  def balance_menu
    [['Bonus', bonus_path], ['History Saldo', history_balance_path]]
    #[['Bonus', bonus_path], ['Transfer Saldo', transfer_path], ['History Saldo', history_balance_path]]
  end
  
  def admin_transaction_menu
    [['Bonus belum dibayarkan', admin_transaction_path], ['Bonus yang dibayarkan', admin_paid_transaction_path]]
  end
  
  def admin_serial_menu
    [['Semua Serial Data', admin_serial_path], ['Serial yang terkirim', admin_serial_terkirim_path], ['Serial yang terpakai', admin_serial_tersedia_path]]
  end
  
  def message_menu
    [['Pesan Masuk', message_inbox_path], ['Pesan Keluar', message_sentbox_path]]
  end
  
  def slug(obj)
    obj.title.downcase.split(' ').join('-') rescue ''
  end
  
  def meta_tag
    str = '<meta name="description" content="beli apa aja, kapan aja dan dimana aja, semuanya hanya di beliapaaja.com">'
    str += '<meta name="keywords" content="Komunitas, Sistem Canggih, Teknik Promosi, Bonus Cepat, beliapaaja, toko online, mall online">'
    str += '<meta name="author" content="beliapaaja.com">'
    return raw str
  end
end
