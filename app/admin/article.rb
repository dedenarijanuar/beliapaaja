ActiveAdmin.register Article do
  permit_params :title, :description, :image

  index do
    selectable_column
    id_column
    
    column "Title" do |article|
      article.title
    end
    column "Description" do |article|
      ActionView::Base.full_sanitizer.sanitize(article.description.truncate(200))
    end
    actions
  end
  
  filter :title

  form(:html => { :multipart => true }) do |f|
    f.inputs "Blog Details" do
      f.input :title
      f.input :description, :as => :ckeditor
      f.input :image, :as => :file
    end
    f.actions
  end
  
  show do |s|
    render "admin/pages/show", page: s
  end
  
end
