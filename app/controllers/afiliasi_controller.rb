class AfiliasiController < ApplicationController
  layout 'page'
  before_filter :all_init
  
  def home
    @captcha = SecureRandom.hex(3).upcase
    @captcha_url = `curl -d 'text=#{@captcha}&font=verdana&fcolor=000000&size=20&bcolor=FFFCFC&type=png' 'http://api.img4me.com'`
    @members = Member.all.order('id DESC').limit(5) rescue []
    @news = Article.all.order('id DESC').limit(4) rescue []
    render :layout => 'afiliasi'
  end
  
  def page
    slug = params[:slug].split('-').join(' ')
    @page = Page.find_by_title(slug)
    return (flash.now[:error] = 'halaman tidak ditemukan' and redirect_to afiliasi_path) unless @page
  end
  
  def sistem_bisnis
    render :layout => 'slide'
  end
  
  def program
    slug = params[:slug].split('-').join(' ')
    @page = Program.find_by_title(slug)
    return (flash.now[:error] = 'halaman tidak ditemukan' and redirect_to afiliasi_path) unless @page
  end
  
  def list_berita
    @news = Article.all.order('id DESC')
    render :layout => 'list_page'
  end
  
  def berita
    slug = params[:slug].split('-').join(' ')
    @page = Article.find_by_title(slug)
    return (flash.now[:error] = 'halaman tidak ditemukan' and redirect_to afiliasi_path) unless @page
  end
  
  def list_agenda
    @agendas = Agenda.all.order('id DESC')
    render :layout => 'afiliasi'
  end
  
  def agenda
    slug = params[:slug].split('-').join(' ')
    @page = Agenda.find_by_title(slug)
    return (flash.now[:error] = 'halaman tidak ditemukan' and redirect_to afiliasi_path) unless @page
  end
  
  def galleries
  end
  
  def faq
  end
  
  def all_init
    @pages = Page.all
    @programs = Program.all
  end
end
