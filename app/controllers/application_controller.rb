class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :current_user
  
  def current_user
    return false unless session[:current_user].present?
    user = SerialData.find_by_serial(session[:current_user].to_s.string_decrypt.split(' ')[0]).member rescue nil
    return false unless user
    @current_user = user
  end
  
end
