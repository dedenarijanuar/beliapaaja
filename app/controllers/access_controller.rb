class AccessController < ApplicationController
  before_filter :authenticate_user
  
  def authenticate_user
    return redirect_to root_path unless @current_user.present?
  end
end
