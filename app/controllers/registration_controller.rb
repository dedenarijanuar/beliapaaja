class RegistrationController < ApplicationController
  before_filter :redirect_to_index, only: [:sign_up, :process_sign_up]
  before_filter :all_init
  layout 'slide'
  
  def sign_up
    @member = Member.new
  end
  
  def process_sign_up
    @member = Member.new(params[:member])
#    if simple_captcha_valid?
      return (flash[:error] = "anda belum menceklist persetujuan" and render :sign_up) if params[:agreement].blank?
      serialdata = SerialData.find_with_serial_and_pin(params[:serial_id], params[:pin_id])
      return (flash[:error] = 'registrasi gagal, no serial dan pin tidak terdaftar dalam database kami' and render :sign_up) unless serialdata
      return (flash[:error] = 'tambah member gagal, no serial dan pin sudah dimiliki user lain' and render :sign_up) if serialdata.member.present?
      if params[:sponsor].present?
        sponsor = SerialData.find_by_serial(params[:sponsor])
        return (flash[:error] = 'registrasi gagal, sponsor tersebut tidak tersedia' and render :sign_up) unless sponsor
        return (flash[:error] = 'registrasi gagal, sponsor tersebut tidak tersedia' and render :sign_up) unless sponsor.member.present?
        upline = SerialData.find_by_serial(params[:upline])
        return (flash[:error] = 'registrasi gagal, upline tersebut tidak tersedia' and render :sign_up) unless upline
        return (flash[:error] = 'registrasi gagal, upline tersebut tidak tersedia' and render :sign_up) unless upline.member.present?
        return (flash[:error] = 'registrasi gagal, upline tersebut sudah penuh, silahkan hubungi sponsor anda atau customer service kami' and render :sign_up) if upline.member.children.count > 1
      end
      @member.serial_data_id = serialdata.id
      @member.sponsor_id = sponsor.id
      @member.parent_id = upline.id
      @member.table_number = upline.member.children.count
      if @member.save
        Balance.get_bonus_sponsor(@member.sponsor_id)
        Balance.get_bonus_prestasi(@member.parent, @member.id)
        flash[:notice] = "registrasi berhasil"
        redirect_to diagram_network_path
      else
        tr = "Tambah member gagal: <br/><br/>"
        @member.errors.full_messages.each do |x|
          str << "#{x} <br/>"
        end
        flash[:error] = str
        render :sign_up
      end
#    else
#      flash[:error] = "Captcha tidak valid" and render :sign_up
#    end
  end
  
  private
    def redirect_to_index
      return (redirect_to diagram_network_path) if @current_user.present?
    end
  
    def all_init
      @pages = Page.all
      @programs = Program.all
    end
end
