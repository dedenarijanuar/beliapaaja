class NetworksController < AccessController
  def my_network
  
  end
  
  def find_network
    @user = Member.find_by_id(params[:member_id]) || @current_user
    @members = @current_user.subtree
  end
  
  def diagram_network
    @user = @current_user
    if params[:member_id].present?
      return (flash[:error] = 'member tidak ditemukan' and redirect_to diagram_network_path) unless @current_user.subtree_ids.include?(params[:member_id].to_i)
      @user = Member.find_by_id(params[:member_id])
    end
    @users = @user.children.limit(10)
  end
  
  def history_network
    @member_left = @current_user.children_left.subtree rescue []
    @member_right = @current_user.children_right.subtree rescue []
    @members = (@member_left + @member_right).group_by{ |x| x.created_at.to_date } rescue []
  end
  
  def add_network
    upline = Member.upline(params[:member_id])
    #return (flash[:error] = 'user tidak ditemukan' and redirect_to diagram_network_path) if [1,2,3,4,6].include?(params[:member_id].to_i)
    return (flash[:error] = 'user tidak ditemukan' and redirect_to diagram_network_path) if upline.children_left.present? && upline.children_right.present?
    return (flash[:error] = 'user tidak ditemukan' and redirect_to diagram_network_path) unless Member.upline(params[:member_id]).present?
    @user = Member.new
  end
  
  def create_network
    @user = Member.new(params[:member])
#    if simple_captcha_valid?
      rp = add_network_path(member_id: params[:member][:parent_id], table_number: params[:member][:table_number])
      return (flash[:error] = "anda belum menceklist persetujuan" and redirect_to rp) if params[:agreement].blank?
      serialdata = SerialData.find_with_serial_and_pin(params[:serial_id], params[:pin_id])
      return (flash[:error] = 'tambah member gagal, no serial dan pin tidak terdaftar dalam database kami' and redirect_to rp) unless serialdata
      return (flash[:error] = 'tambah member gagal, no serial dan pin sudah dimiliki user lain' and redirect_to rp) if serialdata.member.present?
      @user.serial_data_id = serialdata.id
      @user.sponsor_id = @current_user.id
      @user.table_number = params[:member][:table_number]
      if @user.save
        Balance.get_bonus_sponsor(@user.sponsor_id)
        Balance.get_bonus_prestasi(@user.parent, @user.id)
        flash[:notice] = "tambah member berhasil"
        redirect_to diagram_network_path(member_id: params[:member][:parent_id])
      else
        str = "Tambah member gagal: <br/><br/>"
        @user.errors.full_messages.each do |x|
          str << "#{x} <br/>"
        end
        flash[:error] = str
        redirect_to rp
      end
#    else
#      flash[:error] = "Captcha tidak valid" and render :add_member
#    end
  end
  
  def afiliasiku
    
  end
end
