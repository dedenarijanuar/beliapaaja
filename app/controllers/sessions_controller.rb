class SessionsController < ApplicationController
  
  def login
    return redirect_to my_profile_path if @current_user
    return redirect_to admin_serial_path if session[:current_admin].present?
  end
  
  def process_login
    if Captcha::verify_captcha(params[:captcha], params[:captcha_text])
      serialdata = SerialData.where(serial: params[:serial], pin: params[:pin], status: true).first
      if is_super_admin?(params[:serial], params[:pin])
        flash[:notice] = 'anda login sebagai admin'
        session[:current_admin] = 'adafbefbfbsfbisebfibsiefbisbefibsfef'
        session[:owner] = 'adafbefbfbs234234234bise2424fef'
        redirect_to admin_user_path and return
      end
      
#      if is_owner?(params[:serial], params[:pin])
#        flash[:notice] = 'anda login sebagai owner'
#        session[:current_admin] = 'adafbe23434dfbsf23423siefbisfhyjyefibsfef'
#        session[:owner] = 'adafbefbfbs234234234bise2424fef'
#        redirect_to admin_user_path and return
#      end
      
      if serialdata && serialdata.member.present?
        session[:current_user] = serialdata.member.generate_sessions
        flash[:notice] = 'login berhasil'
        redirect_to diagram_network_path
      else
        flash[:error] = 'login gagal'
        redirect_to :back
      end
    else
      flash[:error] = 'login gagal'
      redirect_to :back
    end
  end
  
  def logout
    session[:current_user] = nil
    session[:current_admin] = nil
    session[:owner] = nil
    flash[:notice] = 'logout berhasil'
    redirect_to root_path
  end
  
  private
    def is_super_admin?(serial, pin)
      return serial == 'antim' && pin == 'mrb34n'
    end
    
    def is_owner?(serial, pin)
      return serial == 'nufan' && pin == 'mynameisnufan'
    end
end

