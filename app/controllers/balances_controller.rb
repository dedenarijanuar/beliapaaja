class BalancesController < AccessController
  def bonus
    if params[:bonus_type].present?
      case params[:bonus_type]
      when Bonus::SUPPORT
        redirect_to support_path
      when Bonus::REWARD
        redirect_to reward_path
      end
    end
  end
  
  def transfer
  
  end
  
  def history
  
  end
  
  def support
    @bonus = Bonus.supports
    case params[:bonus_type]
    when Bonus::REWARD
      redirect_to reward_path
    when Bonus::PILIHAN
      redirect_to bonus_path
    end
  end
  
  def reward
    @bonus = Bonus.rewards
    case params[:bonus_type]
    when Bonus::SUPPORT
      redirect_to sponsor_path
    when Bonus::PILIHAN
      redirect_to bonus_path
    end
  end
end
