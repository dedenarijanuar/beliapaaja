class ProfilesController < AccessController

  def show
  end
  
  def edit
  end
  
  def update
    return (flash[:error] = "anda belum menceklist persetujuan" and render :edit) if params[:agreement].blank?
    if @current_user.update_attributes(params[:member])
      flash[:notice] = "ganti profile berhasil"
      redirect_to my_profile_path
    else
      str = "Edit profile gagal: <br/><br/>"
      @current_user.errors.full_messages.each do |x|
        str << "#{x} <br/>"
      end
      flash[:error] = str
      render :edit
    end
  end
  
  def change_pin
  end
  
  def update_pin
    serial = SerialData.find_by_pin(params[:pin_awal])
    return (flash[:error] = 'ganti pin gagal' and render :change_pin) unless serial
    return (flash[:error] = 'ganti pin gagal' and render :change_pin) unless serial.pin == @current_user.serial_data.pin
    return (flash[:error] = 'ganti pin gagal, konfirmasi pin harus sama' and render :change_pin) if params[:pin].to_s != params[:konfirmasi_pin].to_s
    return (flash[:error] = 'ganti pin gagal, pin baru harus 6 digit' and render :change_pin) unless params[:pin].length == 6
    if @current_user.serial_data.update_attributes(pin: params[:pin])
      flash[:notice] = 'ganti pin berhasil'
      redirect_to my_profile_path
    else
      flash[:error] = 'ganti pin gagal' and render :change_pin
    end
  end
end
