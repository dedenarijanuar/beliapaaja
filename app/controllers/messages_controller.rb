class MessagesController < AccessController
  def inbox
    @messages = @current_user.inbox.group_by{|x| x.receiver_id }
  end
  
  def sentbox
    @messages = @current_user.sentbox.group_by{|x| x.receiver_id }
  end
  
  def write
    
  end
  
  def process_write
    receiver = params[:receiver_id].split(' - ').first rescue ''
    serial = SerialData.find_by_serial(receiver)
    return (flash[:error] = "serial tidak valid" and render :write) unless serial
    Message.create_message(params.merge!(sender_id: @current_user.id, receiver_id: serial.member.id ))
    flash[:notice] = "pesan berhasil dikirim" and redirect_to message_inbox_path
  end
  
  def reply
  
  end
  
  def process_reply
  
  end
  
  def show
  
  end
  
  def delete
  
  end
end
