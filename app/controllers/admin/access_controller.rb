class Admin::AccessController < ApplicationController
  before_filter :authenticate_user
  layout 'admin'
  
  def authenticate_user
    return redirect_to admin_index_path unless session[:current_admin].present?
    @current_admin = session[:current_admin]
  end
end
