class Admin::HomeController < Admin::AccessController
  before_filter :find_user, only: [:edit_user, :update_user]
  
  def serial
    return (redirect_to admin_serial_terkirim_path) unless session[:owner].present?
    @serials = SerialData.all.order('id ASC')
  end
  
  def serial_terkirim
    @serials = SerialData.where(terkirim: 1).order('id ASC')
  end
  
  def serial_tersedia
    @serials = SerialData.all.order('id ASC').keep_if{|x| x.member.present? }
  end
  
  def serial_dikirim
    return (flash[:error] = "serial gagal dikirim" and redirect_to admin_serial_path) unless params[:serial].present?
    member = SerialData.find(params[:serial].keys)
    member.each do |m|
      m.update_attributes(terkirim: 1, waktu_terkirim: Date.today)
    end
    flash[:notice] = "serial berhasil dikirim" and redirect_to admin_serial_path
  end
  
  def user
    @members = Member.all.order('id ASC')
  end
  
  def transaction
    @members = Member.all.order('id DESC').keep_if{|x| x.total_balance > 0 }
  end
  
  def paid_transaction
    @members = Member.all.order('id DESC')
  end
  
  def transfer_saldo
    return (flash[:error] = "transfer gagal" and redirect_to admin_transaction_path) unless params[:members].present?
    member = Member.find(params[:members].keys)
    member.each do |m|
      m.balances.destroy_all
    end
    flash[:notice] = "transfer berhasil" and redirect_to admin_transaction_path
  end
  
  def edit_user
    @user = Member.find_by_id(params[:id])
    
  end
  
  def update_user
    return (flash[:error] = "anda belum menceklist persetujuan" and render :edit_user) if params[:agreement].blank?
    if @user.update_attributes(params[:member])
      flash[:notice] = "ganti profile user berhasil"
      redirect_to admin_edit_user_path(@user)
    else
      str = "Edit profil member gagal: <br/><br/>"
        @user.errors.full_messages.each do |x|
          str << "#{x} <br/>"
        end
        flash[:error] = str
      render :edit_user
    end
  end
  
  def find_user
    @user = Member.find_by_id(params[:id])
    return (flash[:error] = "user tidak ditemukan" and redirect_to admin_edit_user_path(@user) ) unless @user
  end
end
