class User < ActiveRecord::Base
  #apply_simple_captcha message: "Captcha tidak valid", :add_to_base => true
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  attr_accessible :email, :password, :password_confirmation
  
  def member
    Member.first
  end
end
