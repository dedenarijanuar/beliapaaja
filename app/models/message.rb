class Message < ActiveRecord::Base
  belongs_to :member
  attr_accessible :sender_id, :receiver_id, :subject, :content, :is_read, :deleted_by_sender, :deleted_by_receiver
  #belongs_to :receiver, :foreign_key => 'id', :class_name => "Member"
  #belongs_to :sender, :foreign_key => 'sender_id', :class_name => "Message"
  
  def self.all_receiver
    Member.all.map{|x| "#{x.serial} - #{x.nama}"}.join('_')
  end
  
  def self.create_message(params)
    self.create(sender_id: params[:sender_id], receiver_id: params[:receiver_id], subject: params[:subject], content: params[:content], is_read: false)
  end
  
  def self.reply_message(params)
    self.create(sender_id: params[:sender_id], receiver_id: params[:receiver_id], content: params[:content], is_read: false, parent_id: params[:parent_id])
  end
  
  def update_readable
    self.update_attributes(is_read: true)
  end
  
  def delete_inbox(params)
    Message.find(params[:inbox_ids].keys).each do |m|
      m.update_attributes(deleted_by_receiver: true)
    end
  end
  
  def delete_sentbox(params)
     Message.find(params[:inbox_ids].keys).each do |m|
      m.update_attributes(deleted_by_sender: true)
    end
  end
  
  def receiver
    SerialData.find_by_id(self.receiver_id).member.nama rescue 'Admin'
  end
  
  def sender
    SerialData.find_by_id(self.sender_id).member.nama rescue 'Admin'
  end
end
