class Bonus < ActiveRecord::Base
  attr_accessible :total_left, :total_right, :bonus_type, :name
  SUPPORT = 'B.Support'
  REWARD = 'B.Reward'
  PILIHAN = 'Pilihan'
  SPONSOR = 'B.Sponsor'
  PRESTASI = 'B.Prestasi'
  MATCHING = 'B.Matching'
  
  scope :supports, -> { where(bonus_type: SUPPORT) }
  scope :rewards, -> { where(bonus_type: REWARD) }
  
  def self.options
    [PILIHAN,SPONSOR, PRESTASI, MATCHING, SUPPORT,REWARD]
  end

  def self.create_seed_all_bonus
    total_support = [7, 25, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
    support_names = [
      'royalty 1','royalty 2','royalty 3','royalty 4 + tour ke Bali',
      'Cash Rp. 10.000.000','Cash Rp. 15.000.000', 'Cash Rp. 20.000.000',
      'Cash Rp. 25.000.000','Cash Rp. 30.000.000', 'Cash Rp. 35.000.000',
      'Cash Rp. 40.000.000', 'Cash Rp. 45.000.000', 'Cash Rp. 50.000.000 + Royalti 5'
    ]
    
    total_reward = [3000, 6000, 12000]
    reward_names = ['Honda Mobilio Cash Rp. 175.000.000', 'Tabungan Rp. 250.000.000','Rumah Rp. 500.000.000 + 3% saham perusahaan']
    total_support.each_with_index do |t, i|
      Bonus.create(total_left: t, total_right: t, bonus_type: SUPPORT, name: support_names[i])
    end
    
    total_reward.each_with_index do |t, i|
      Bonus.create(total_left: t, total_right: t, bonus_type: REWARD, name: reward_names[i])
    end
  end
end
