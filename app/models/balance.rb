class Balance < ActiveRecord::Base
  paranoid
  SPONSOR = 'b.sponsor'
  PRESTASI = 'b.prestasi'
  MATCHING = 'b.matching'
  belongs_to :member
  attr_accessible :get_at, :balance_type, :amount, :member_id
  
  def self.get_bonus_sponsor(sponsor_id)
    self.create(get_at: Date.today, balance_type: SPONSOR, amount: 50000, member_id: sponsor_id)
  end
  
  def self.get_bonus_prestasi(parent, child_id)
    child = Member.find_by_id(child_id)
    
    if child.table_number == 1
      if child.parent.table_number == 0
        Balance.create(get_at: Date.today, balance_type: PRESTASI, amount: 50000, member_id: child.parent.parent.id) rescue false
        Balance.create(get_at: Date.today, balance_type: MATCHING, amount: 50000, member_id: child.parent.parent.sponsor_id) rescue false
      else
        bonus = []
        child.parent.ancestors.reverse.each do |x|
          bonus << x
          break if x.table_number == 0
        end
        user_id = bonus.last.id
        matching = Member.find_by_id(user_id).parent
        Balance.create(get_at: Date.today, balance_type: PRESTASI, amount: 50000, member_id: matching.id) rescue false
        Balance.create(get_at: Date.today, balance_type: MATCHING, amount: 50000, member_id: matching.sponsor_id) rescue false
      end
    end
  end
  
end
