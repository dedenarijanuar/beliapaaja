class SerialData < ActiveRecord::Base
  attr_accessible :pin, :serial, :status, :terkirim, :waktu_terkirim
  #before_create :generate_pin
  #after_create :generate_serial
  has_one :member, dependent: :destroy
  default_scope { order('id ASC') }
  validates :serial, uniqueness: true
  
  def self.find_with_serial_and_pin(serial, pin)
    serial = self.where(serial: serial, pin: pin, status: true).first
    serial.member.present? ? nil : serial rescue nil
  end
  
#  def generate_pin
#    return if self.pin.present?
#    self.pin = SecureRandom.hex(3).upcase
#  end

#  def generate_serial
#    return if self.serial.present?
#    self.serial = "BB#{SecureRandom.hex(2).upcase}"
#    self.save
#  end
  
  def self.generate_serial_data_seeds(number)
    number.to_i.times do |n|
      SerialData.create(
        pin: SecureRandom.hex(3).upcase, #'888888', #
        serial: "EMI#{SecureRandom.hex(3).upcase}",
        status: true
      )
    end
  end
end
