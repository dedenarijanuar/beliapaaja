class Page < ActiveRecord::Base
  attr_accessible :title, :description, :image
  mount_uploader :image, ImageUploader
  
  def self.generate_seeds
    pages = {'Tentang Kami' =>
      '<p><span style="font-size:16px"><strong><img alt="" src="/uploads/ckeditor/pictures/1/content_logo.png" style="float:left; height:172px; margin-left:10px; margin-right:10px; width:250px" /></strong></span></p>

      <p><span style="font-size:16px"><strong>Apa itu beliapaaja.com ?</strong></span></p>

      <p style="text-align:justify"><span style="font-size:14px">Beliapaaja.com adalah e-commerce pertama di Indonesia yang memadukan konsep mall online dengan afiliasi program (Network Marketing). Kini anda bisa mendapatkan keuntungan lebih dari sekedar bertransaksi dalam dunia online.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Pertumbuhan pengguna online dan transaksi secara online kini semakin bertambah setiap detiknya, Jika anda dapat melihat peluang ini maka beliapaaja.com menjadi jawaban yang tepat untuk patner bisnis anda.</span></p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><span style="font-size:16px"><strong>Bagaimana Program Bisnis ini dikembangkan?</strong></span></p>

      <p style="text-align:justify"><span style="font-size:14px">Manajemen beliapaaja.com menyadari bahwa keseimbangan hidup perlu di jaga dengan baik, melalui program afiliasi ini bukan hanya teknologi yang harus bertumbuh akan tetapi kualitas hidup dan kesehatan para pengguna <em>account</em> beliapaaja.com menjadi sorotan utama dalam bisnis ini.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Maka dari itu manajemen mengembangkan pula produk Air Terapi untuk memenuhi kebutuhan kesehatan para agen/mitra usaha dan pengguna account beliapaaja.com. Kami menyebutnya perpaduan antara bisnis dari Alam dan Teknologi, yang akan memberikan Anda kualitas hidup dan kesuksesan yang lebih baik.</span></p>

      <p style="text-align:justify">&nbsp;</p>

      <hr />
      <p style="text-align:justify"><span style="font-size:16px"><strong>Legalitas Usaha</strong></span></p>

      <p style="text-align:justify"><span style="font-size:14px">Program afiliasi beliapaaja.com merupakan salah satu inovasi produk dari PT.Esa Mulia Indonesia. Beliapaaja.com di dirikan pada tanggal 14 April 2014, dengan memadukan mall online dan program afiliasi yang bisa di jalankan seluruh masyarakat dengan online maupun offline.&nbsp;</span></p>

      <ul>
	      <li>KEMENHUM : AHU-55592.AH.01.01.Tahun 2013</li>
	      <li>SIUP : 510/3-E73/BPPT</li>
	      <li>TDP : 101114619952</li>
	      <li>NPWP : 03.347.151.7-429.000&nbsp;</li>
      </ul>','Sambutan' =>
      '<p><img alt="" src="/uploads/ckeditor/pictures/2/content_FOTO_PA_SONI_SAMBUTAN.png" style="float:left; height:215px; margin-left:10px; margin-right:10px; width:200px" /></p>

      <p style="text-align:justify"><span style="font-size:14px"><em>Bismillahirrohmannirrohim.. Assallamu&#39;alaikum wr &nbsp;wb.</em><br />
      <strong>Salam sejahtera bagi kita semua</strong></span></p>

      <p style="text-align:justify"><span style="font-size:14px">Selamat datang dan selamat bergabung dengan keluarga besar beliapaaja.com . Program bisnis ini merupakan sebuah inovasi terbaru dan dari <strong>PT.ESA MULIA INDONESIA</strong><br />
      Bersama beliapaaja.com kami menghadirkan bisnis untuk Anda yang <strong>CEPAT &nbsp;DAN MENGUNTUNGKAN</strong></span></p>

      <p style="text-align:justify"><span style="font-size:14px">Kesuksesan para mitra afiliasi &nbsp;juga merupakan inti kesuksesan <strong>PT. ESA MULIA INDONESIA</strong> , sehingga seluruh jajaran manajemen dan karyawan berkomitmen untuk memberikan kemudahan dalam setiap aktivitas bisnis, serta senantiasa menciptakan dan memelihara hubungan kerja yang baik berlandaskan prinsip saling percaya untuk mencapai visi bersama.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Kami percaya beliapaaja.com &nbsp;bersama semua Mitranya bisa memberikan kontribusi kepada berbagai pihak , dengan menciptakan &ldquo; Keluarga yang sejahtera bagi semua lapisan Masyarakat Indonesia melalui sistem yang berkualitas &amp; terjangkau serta dengan sistem usaha yang terus berkesinambungan&rdquo;.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Semoga niat kami ini dapat mewujudkan keinginan kita semua dan dapat dirasakan secara nyata perubahannya baik secara ekonomi, sosial, financial dan spiritual. Marilah kita bekerja bersama-sama dengan sepenuh hati dan diiringi dengan do&#39;a, rendah hati, penuh simpati serta merasakan empati.<br />
      Siapapun anda dan apapun latar belakang anda mari bersama tumbuh dan berkembang dengan sistem yang tepat untuk kehidupan yang lebih baik.<br />
      Terima kasih dan sukses selalu Salam Perubahan.. !</span></p>

      <p style="text-align:justify"><span style="font-size:14px"><strong>Soni Sudrajat,SE</strong><br />
      CEO PT.Esa Mulia Indonesia</span></p>
      ','Visi & Misi' =>
      '<p><span style="font-size:16px"><strong><img alt="" src="/uploads/ckeditor/pictures/3/content_visi-misi-smpn-114-jkt.jpg" style="float:left; height:235px; width:300px" /></strong></span></p>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p><span style="font-size:16px"><strong>VISI</strong></span><br />
      <span style="font-size:14px">Membangun sumber daya manusia secara profesional dengan rendah hati, penuh simpati, merasakan empati, beriman,bertaqwa dan ber-ketuhanan yang maha ESA</span></p>

      <p><strong><span style="font-size:16px">MISI</span></strong><br />
      <span style="font-size:14px">Menyediakan fasilitas usaha yang nyata serta membantu masyarakat untuk berani membangun suatu usaha yang transparan, aman, nyaman, dan tidak merugikan. Membantu masyarakat yang ingin membangun suatu usaha tetapi belum memiliki potensi yang cukup&nbsp;</span></p>

      <p><strong><span style="font-size:16px">NILAI BUDAYA</span></strong></p>

      <ul>
	      <li>Saling Menjaga Kepercayaan&nbsp;</li>
	      <li>Bersama Membangun Integritas&nbsp;</li>
	      <li>Bekerja Secara Profesional&nbsp;</li>
	      <li>Fokus Pada Visi dan Misi Bersama&nbsp;</li>
	      <li>Memberikan Pelayanan Yang Maksimal&nbsp;</li>
      </ul>
      ', 'Apa itu afiliasi' =>
      '
      <p><img alt="" src="/uploads/ckeditor/pictures/4/content_Network.jpg" style="float:right; height:228px; margin-left:10px; margin-right:10px; width:500px" /><span style="font-size:14px">Seringkali kita mendengar kata afiliasi baik itu di tv, di koran, di majalah atau berbagai sumber lainnya, namun kita belum tahu sebenarnya apa itu afiliasi ?</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Dalam dunia perdagangan, ada pembeli dan ada penjual. Jika Anda pernah mendengar ada sebuah perusahaan dagang yang menjual peralatan masak, lalu ketika salah seorang teman Anda sedang mencari peralatan masak dan Anda mereferensikan kepada teman Anda untuk membeli di perusahaan tersebut, maka Anda akan mendapatkan komisi setelah teman Anda tersebut melakukan pembelian di perusahaan tersebut. Mengapa Anda mendapatkan komisi dari perusahaan tersebut? Karena Anda telah mereferensikan produk perusahaan tersebut kepada teman Anda, sehingga teman Anda akhirnya membeli produk dari perusahaan tersebut. Itulah yang dinamakan AFILIASI</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Jadi AFILIASI itu adalah &ldquo;bentuk kerjasama yang kita jalin dengan perusahaan atau lembaga pemilik produk (affiliate merchant) untuk memasarkan produk mereka dan sebagai pemasar produk (affiliate marketers), kita (hanya) dibayar setelah ada produk terjual&rdquo;.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Di program beliapaaja.com anda dapat menjalankan program afiliasi layaknya anda berbisnis jaringan, dimana anda cukup mereferensikan siapapun untuk bergabung dalam program ini dan ajarkan kepada mereka untuk melakukan hal yang sama seperti anda.<br />
      Setiap orang di beliapaaja.com akan di ajarkan bagaimana menjadi seorang pebisnis afiliasi yang benar dengan kurikulum pendidikan bisnis yang telah dibuat serta anda bebas untuk mengembangkan bisnis yang anda punya untuk di promosikan kepada seluruh dunia melalui web beliapaaja.com maupun took online yang anda miliki.</span></p>

      <p style="text-align:justify"><span style="font-size:14px">Nikmati cara baru dalam era berbisnis jaringan, dan bersiaplah untuk meraih kebebasan financial yang lebih nyata.</span></p>
    '}

    pages.each do |x,y|
      Page.create(title: x, description: y)
    end

  end
end
