class Collect
  def self.values( hashes )
    # Requires Ruby 1.8.7+ for Object#tap
    Hash.new{ |h,k| h[k]=[] }.tap do |result|
      hashes.each{ |h| h.each{ |k,v| result[k] << v } }
    end    
  end
end
