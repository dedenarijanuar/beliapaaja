class Member < ActiveRecord::Base
  #ply_simple_captcha message: "Captcha tidak valid", :add_to_base => true
  has_ancestry
  belongs_to :serial_data
  has_many :balances
  has_many :inbox, :foreign_key => 'receiver_id', :class_name => "Message"
  has_many :sentbox, :foreign_key => 'sender_id', :class_name => "Message"
  
  attr_accessible :is_active, :nama, :tempat_lahir, :tanggal_lahir, :agama, :ibu_kandung, :parent_id,
    :alamat, :kode_pos, :kota, :negara, :telepon, :jenis_kelamin, :pekerjaan, :status_sipil,
    :handphone, :email, :no_identitas, :ahli_waris, :nama_bank, :nama_pemilik, :email, :role_id,
    :no_rekening, :serial_data_id, :hubungan, :captcha, :captcha_key, :kewarganegaraan, :provinsi, :no_npwp,
    :cabang, :sponsor_id, :table_number, :is_karyawan, :nick_name, :is_seed
  
  attr_accessor :is_seed
   #after_create :set_default_karyawan
   
#  after_create :update_serial_data
  validates :nama, :ibu_kandung, #:tempat_lahir, :tanggal_lahir, :hubungan, :no_ktp, :alamat, :ahli_waris,
    :nama_bank, :nama_pemilik, :no_rekening, 
    :handphone, presence: {message: 'tidak boleh kosong'}, unless: :is_seed?
  validates :kode_pos, :telepon, :handphone, :no_identitas, :no_rekening,
    numericality: {message: 'harus di isi dengan angka'}, allow_blank: true
#    
  validates :email, format: {message: 'format salah', with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}, unless: :is_seed? #, allow_blank: true
#  validates :serial_data_id, uniqueness: {message: 'serial dan pin sudah terdaftar'}#, allow_blank: true  
  
#  def set_default_karyawan
#    self.update_attributes(is_karyawan: true) if self.id <= 100
#    return  
#  end

  def is_seed?
    self.is_seed
  end
  
  def nama_panggilan
    return self.nick_name if self.nick_name.present?
    self.nama
  end
  
  def total_balance
    admin_cost = 5000
    balance = self.balances.map{|x| x.amount}.sum
    return 0 if balance < 1
    (balance - admin_cost)
  end
  
  def total_balance_without_admin
    balance = self.balances.map{|x| x.amount}.sum
    return 0 if balance < 1
    balance
  end
  
  def total_paid_balance
    self.balances.only_deleted.group_by{|x| x.deleted_at}
  end
  
  def is_admin?
    self.serial_data.id == 1
  end
  
  def serial
    self.serial_data.serial rescue '-'
  end
  
  def sponsor
    Member.find_by_id(self.sponsor_id) rescue nil
  end
  
  def self.upline(upline_id)
    Member.find_by_id(upline_id) rescue nil
  end
  
  def self.bonus_sponsors
    get_bonus = []
    Member.all.each do |m|
      next if m.sponsor.blank?
      next if m.sponsor.id <= 100
      get_bonus << { [m.sponsor.nama, m.sponsor.serial, m.sponsor.id] => m.id }
    end
    
    Collect.values(get_bonus).sort
  end
  
#  def status
#    self.is_active? ? self.serial_data.status? ? 'aktif' : 'pasif' : "diblokir, <a href='/admin/#{self.id}/activated_member'>aktifkan?</a>".html_safe
#  end
  
  def pin
    self.serial_data.pin
  end
   
  def generate_sessions
    "#{self.serial_data.serial} #{self.nama} #{self.serial_data.pin}".string_encrypt
  end
  
#  def update_serial_data
#    serialdata = SerialData.find_by_id(self.serial_data_id)
#    serial_data.update_attributes(status: true)
#  end
  
  def members
    members = []
    levels = []
    self.descendants.select(:id, :ancestry).each do |member|
      levels << { member.ancestry.split('/').size => member.id }
    end
    
    Collect.values(levels).sort
  end
  
  def self.create_users_seed
    Member.delete_all
    Member.create(nama: 'Perusahaan 1', serial_data_id: SerialData.find(1).id, table_number: 0, is_seed: true)
    Member.create(nama: 'Perusahaan 2', serial_data_id: SerialData.find(2).id, parent_id: 1, table_number: 0, sponsor_id: 1, is_seed: true)
    Member.create(nama: 'Perusahaan 3', serial_data_id: SerialData.find(3).id, parent_id: 2, table_number: 0, sponsor_id: 2, is_seed: true)
    Member.create(nama: 'Sony 1', serial_data_id: SerialData.find(4).id, parent_id: 3, table_number: 0, sponsor_id: 3, is_seed: true)
    Member.create(nama: 'IT 1', serial_data_id: SerialData.find(5).id, parent_id: 3, table_number: 1, sponsor_id: 3, is_seed: true)
    
    #SONY
    Member.create(nama: 'Sony 2', serial_data_id: SerialData.find(6).id, parent_id: 4, table_number: 0, sponsor_id: 4, is_seed: true)
    Member.create(nama: 'Sony 3', serial_data_id: SerialData.find(7).id, parent_id: 4, table_number: 1, sponsor_id: 4, is_seed: true)
    Member.create(nama: 'Sony 4', serial_data_id: SerialData.find(8).id, parent_id: 6, table_number: 0, sponsor_id: 6, is_seed: true)
    Member.create(nama: 'Sony 5', serial_data_id: SerialData.find(9).id, parent_id: 6, table_number: 1, sponsor_id: 6, is_seed: true)
    Member.create(nama: 'Sony 6', serial_data_id: SerialData.find(10).id, parent_id: 7, table_number: 0, sponsor_id: 7, is_seed: true)
    Member.create(nama: 'Sony 7', serial_data_id: SerialData.find(11).id, parent_id: 7, table_number: 1, sponsor_id: 7, is_seed: true)
    
    Member.create(nama: 'Nurhayati', serial_data_id: SerialData.find(12).id, parent_id: 10, table_number: 0, sponsor_id: 10, is_seed: true)
    Member.create(nama: 'Eis', serial_data_id: SerialData.find(13).id, parent_id: 12, table_number: 0, sponsor_id: 12, is_seed: true)
    Member.create(nama: 'Naomi', serial_data_id: SerialData.find(14).id, parent_id: 13, table_number: 0, sponsor_id: 13, is_seed: true)
    Member.create(nama: 'Beny', serial_data_id: SerialData.find(15).id, parent_id: 14, table_number: 0, sponsor_id: 14, is_seed: true)
    Member.create(nama: 'Juli Yuliadi', serial_data_id: SerialData.find(16).id, parent_id: 15, table_number: 0, sponsor_id: 15, is_seed: true)
    Member.create(nama: 'Husni', serial_data_id: SerialData.find(17).id, parent_id: 16, table_number: 0, sponsor_id: 16, is_seed: true)
    Member.create(nama: 'Fidi', serial_data_id: SerialData.find(18).id, parent_id: 17, table_number: 0, sponsor_id: 17, is_seed: true)
    Member.create(nama: 'Robin', serial_data_id: SerialData.find(19).id, parent_id: 18, table_number: 0, sponsor_id: 18, is_seed: true)
    
#    Member.create(nama: 'Sony 8', serial_data_id: SerialData.find(12).id, parent_id: 8, table_number: 0, sponsor_id: 8)
#    Member.create(nama: 'Sony 9', serial_data_id: SerialData.find(13).id, parent_id: 9, table_number: 0, sponsor_id: 9)
#    Member.create(nama: 'Sony 10', serial_data_id: SerialData.find(14).id, parent_id: 10, table_number: 0, sponsor_id: 10)
#    Member.create(nama: 'Sony 11', serial_data_id: SerialData.find(15).id, parent_id: 11, table_number: 0, sponsor_id: 11)
#    Member.create(nama: 'Sony 12', serial_data_id: SerialData.find(16).id, parent_id: 12, table_number: 0, sponsor_id: 12)
#    Member.create(nama: 'Sony 13', serial_data_id: SerialData.find(17).id, parent_id: 13, table_number: 0, sponsor_id: 13)
#    Member.create(nama: 'Sony 14', serial_data_id: SerialData.find(18).id, parent_id: 14, table_number: 0, sponsor_id: 14)
#    Member.create(nama: 'Sony 15', serial_data_id: SerialData.find(19).id, parent_id: 15, table_number: 0, sponsor_id: 15)
    
    
    #IT
    Member.create(nama: 'IT 2', serial_data_id: SerialData.find(20).id, parent_id: 5, table_number: 0, sponsor_id: 5, is_seed: true)
    Member.create(nama: 'IT 3', serial_data_id: SerialData.find(21).id, parent_id: 5, table_number: 1, sponsor_id: 5, is_seed: true)
    Member.create(nama: 'IT 4', serial_data_id: SerialData.find(22).id, parent_id: 20, table_number: 0, sponsor_id: 20, is_seed: true)
    Member.create(nama: 'IT 5', serial_data_id: SerialData.find(23).id, parent_id: 20, table_number: 1, sponsor_id: 20, is_seed: true)
    Member.create(nama: 'IT 6', serial_data_id: SerialData.find(24).id, parent_id: 21, table_number: 0, sponsor_id: 21, is_seed: true)
    Member.create(nama: 'IT 7', serial_data_id: SerialData.find(25).id, parent_id: 21, table_number: 1, sponsor_id: 21, is_seed: true)
    Member.create(nama: 'IT 8', serial_data_id: SerialData.find(26).id, parent_id: 22, table_number: 0, sponsor_id: 22, is_seed: true)
    Member.create(nama: 'IT 9', serial_data_id: SerialData.find(27).id, parent_id: 23, table_number: 0, sponsor_id: 23, is_seed: true)
    Member.create(nama: 'IT 10', serial_data_id: SerialData.find(28).id, parent_id: 24, table_number: 0, sponsor_id: 24, is_seed: true)
    Member.create(nama: 'IT 11', serial_data_id: SerialData.find(29).id, parent_id: 25, table_number: 0, sponsor_id: 25, is_seed: true)
    Member.create(nama: 'IT 12', serial_data_id: SerialData.find(30).id, parent_id: 26, table_number: 0, sponsor_id: 26, is_seed: true)
    Member.create(nama: 'IT 13', serial_data_id: SerialData.find(31).id, parent_id: 27, table_number: 0, sponsor_id: 27, is_seed: true)
    Member.create(nama: 'IT 14', serial_data_id: SerialData.find(32).id, parent_id: 28, table_number: 0, sponsor_id: 28, is_seed: true)
    Member.create(nama: 'IT 15', serial_data_id: SerialData.find(33).id, parent_id: 29, table_number: 0, sponsor_id: 29, is_seed: true)
    
    #BENGKULU
    Member.create(nama: 'Boni 1', serial_data_id: SerialData.find(34).id, parent_id: 8, table_number: 0, sponsor_id: 8, is_seed: true)
    Member.create(nama: 'Boni 2', serial_data_id: SerialData.find(35).id, parent_id: 34, table_number: 0, sponsor_id: 34, is_seed: true)
    Member.create(nama: 'Boni 3', serial_data_id: SerialData.find(36).id, parent_id: 34, table_number: 1, sponsor_id: 34, is_seed: true)
    Member.create(nama: 'Boni 4', serial_data_id: SerialData.find(37).id, parent_id: 35, table_number: 0, sponsor_id: 35, is_seed: true)
    Member.create(nama: 'Boni 5', serial_data_id: SerialData.find(38).id, parent_id: 35, table_number: 1, sponsor_id: 35, is_seed: true)
    Member.create(nama: 'Boni 6', serial_data_id: SerialData.find(39).id, parent_id: 36, table_number: 0, sponsor_id: 36, is_seed: true)
    Member.create(nama: 'Boni 7', serial_data_id: SerialData.find(40).id, parent_id: 36, table_number: 1, sponsor_id: 36, is_seed: true)
    #COMMENT ME IF STABLE
#    no = 6
#    number = 100
#    number.to_i.times do |i|
#      i = i+1
#      next if [1,2,3].include?(i) #[1,2,3,4,6]
#      2.times do |n|
#        name = Faker::Name.first_name
#        user = Member.new(
#          nama: name, parent_id: i, serial_data_id: no, table_number: n, sponsor_id: [5,7].sample,
#          tanggal_lahir: Time.now, no_identitas: Faker::Number.number(10), kewarganegaraan: 'indonesia',
#          status_sipil: 'belum menikah', agama: Agama::AGAMA.sample, telepon: Faker::Company.duns_number,
#          ahli_waris: Faker::Name.first_name, ibu_kandung: Faker::Name.first_name, hubungan: 'anak',
#          kode_pos: '12345', handphone: Faker::Company.duns_number, no_npwp: Faker::Number.number(10),
#          tempat_lahir: 'test', email: Faker::Internet.email, alamat: Faker::Address.street_address, provinsi: 'Jawa',
#          nama_bank: 'BCA', nama_pemilik: 'test', no_rekening: Faker::Number.number(10)
#        )
#        user.save(validate: false)
#        no = no+1
#      end
#    end
  end
  
  #perhitungan bonus prestasi yang didapat
  def get_bonus_prestasi
    return 0 if self.children.blank?
    bonus = 0
    self.children.first.get_last_children(bonus)
  end

  #mendapatkan nilai member yang dikiri
  def get_last_children(bonus)
    if self.children.find_by_table_number(1).present?
      bonus = bonus + 50000
      self.children.find_by_table_number(1).get_last_children(bonus)
    else
      return bonus
    end
  end
  
  def get_bonus_matching
    return 0 if self.children.blank?
    self.children.map{|c| c.get_bonus_prestasi }.sum
  end

  def get_total_left
    self.children_left.descendants.count + 1 rescue 0
  end
  
  def get_total_right
    self.children_right.descendants.count + 1 rescue 0
  end

  def children_left
    self.children.find_by_table_number(0)
  end
  
  def children_right
    self.children.find_by_table_number(1)
  end
  
  def afiliasiku
    Member.where(sponsor_id: self.id)
  end
  
  def icon
    self.jenis_kelamin?? '/assets/male.jpg' : '/assets/female.jpg'
  end
  
end
