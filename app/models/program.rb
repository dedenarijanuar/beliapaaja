class Program < ActiveRecord::Base
  attr_accessible :title, :description, :image
  mount_uploader :image, ImageUploader
  
  def self.generate_seeds
    programs = { 'Air Kesehatan' =>
      '<p style="text-align:justify"><img alt="" src="/uploads/ckeditor/pictures/5/content_image_winwater.jpg" style="float:left; height:256px; margin-left:10px; margin-right:10px; width:400px" /></p>

      <p style="text-align:justify">Win Air adalah air kesehatan sangat baik untuk di konsumsi baik untuk mengobati dan menjaga kesehatan kita, minum win air setiap hari membantu menjaga keseimbangan kehidupan anda dan keluarga.</p>

      <p style="text-align:justify">Dengan khasiat yang sangat baik bagi kesehatan dan sertifikat Nasional serta Internasional yang telah di dapat, Win Air menjadi salah satu produk utama program afiliasi beliapaaja.com&nbsp;</p>

      <p style="text-align:justify"><strong>SEJARAH</strong><br />
      Tahun 2009, satu team peneliti yang dipimpin oleh ahli geologi alumni Institut Teknologi Bandung melakukan penelitian terhadap air minum. Konsepnya adalah dikarenakan tubuh kita didominasi oleh air maka sudah seharusnya kita minum air yang bersih, sehat dan menyehatkan. Kita pasti minum air setiap hari, oleh karena itu perlu suatu produk air minum yang selain menghilangkan haus juga dapat membantu banyak masalah kesehatan.</p>

      <p style="text-align:justify">Tahun 2009, selama 8 bulan, kami melakukan pengolahan air minum. Hasil olahannya diuji coba kepada beberapa orang yang sedang mempunyai masalah kesehatan seperti asam urat, kholesterol, darah tinggi, asma, gula darah, stroke bahkan ambeien dan bau badan. Kami sangat gembira, mereka menyatakan bahwa dengan minum air ini penyakitnya dapat teratasi dan sembuh.</p>

      <p style="text-align:justify">Berdasarkan pengalaman itulah, kami mendirikan suatu industri pengolahan air minum yang tentunya dalam proses produksi mengikuti aturan yang ada di Indonesia, Standard Nasional Indonesia (SNI No. 01-2553-2006), merk terdaftar adalah Winair, Badan POM No. MD 249128001041.</p>

      <hr />
      <p style="text-align:justify"><strong>PROSES PENGOLAHAN</strong><br />
      Pengolahan air minum ini terinspirasi oleh air yang keluar sebagai mata air di berbagai daerah di Indonesia yang mempunyai khasiat. Di beberapa daerah saya sering menemukan mata air yang diklaim oleh warganya dapat mengobati penyakit. Atau sebagai contoh adalah air zamzam. Air zamzam adalah air yang keluar di Mekah sebagai mata air.</p>

      <p style="text-align:justify">Dari pandangan geologi kualitas air yang keluar sebagai mata air sangat dipastikan dipengaruhi oleh batuan yang dilewati sebelum keluar sebagai mata air. Air tanah yang melewati batuan kapur dipastikan akan tinggi kadar Calciumnya, begitu juga air tanah yang melewati batuan yang tinggi mineral Kaliumnya maka air tersebut akan tinggi mineral Kaliumnya.</p>

      <p style="text-align:justify">Kejadian di alam ini saya lakukan di laboratorium dimana air dilewatkan ke berbagai macam batuan sesuai kandungan mineral yang dibutuhkan oleh tubuh. Tentu batuannya diolah dahulu agar pelepasan mineralnya lebih cepat apabila air melewatinya.</p>

      <p style="text-align:justify">Batuan yang kami olah adalah :</p>

      <ul>
	      <li style="text-align:justify">Batuan yang tinggi mineral alkali-nya &nbsp;( Kalium, Calcium, Magnesium dan Natrium)</li>
	      <li style="text-align:justify">Mineral lainnya walaupun sedikit tapi diperlukan (Trace Mineral)</li>
	      <li style="text-align:justify">Batuan yang mengandung mineral yang dapat memancarkan infra merah</li>
	      <li style="text-align:justify">Batuan Biofir, Bioenergy</li>
	      <li style="text-align:justify">Batuan bersifat magnet (Magnetit)</li>
      </ul>

      <p style="text-align:justify">Mata air yang kami ambil adalah dari &nbsp;kaki Gunung Palasari Kabupaten Bandung Barat. Ditarik melalui pipa sejauh 8 Km ke pabrik pengolahan. Di Pabrik air ditampung kemudian diproses dalam &nbsp;tahapan pertama yakni Filterisasi dan Sterilisasi. Media filterisasi adalah pasir silika, karbon aktif dan media lainnya sehingga air bebas dari larutan halus (pasir, lumpur, sisa daun dll), pestisida dan logam berat. Sterlisasi menggunakan Ozonisasi sehingga bakteri, jamur &nbsp;mati.</p>

      <p style="text-align:justify">Kemudian air masuk ke tahapan kedua yakni pengaktifan melalui batuan yang sudah diolah agar kandungan mineralnya naik, pH nya naik, mengandung trace mineral dan bioenergy. Melalui teknologi nano maka air aktif ini bersifat : antioksidan. alkaline (pH basa)<br />
      molekulnya kecil (micro molecule), bio-energy, hexagonal dan aktif.<br />
      Tahapan ketiga adalah proses pembotolan. Tahapan ini mengikuti prosedur Standar Nasional Indonesia (SNI-01-3553-2006)/ Mesin pembotolan otomatis buatan jepang.</p>

      <p style="text-align:center"><a href="http://banyusehat.files.wordpress.com/2014/05/proses1.jpg"><img alt="proses1" class="aligncenter size-medium wp-image-21" src="http://banyusehat.files.wordpress.com/2014/05/proses1.jpg?w=300&amp;h=230" style="height:230px; width:300px" /></a><a href="http://banyusehat.files.wordpress.com/2014/05/proses2.jpg" style="line-height: 1.6;"><img alt="proses2" class="aligncenter size-medium wp-image-23" src="http://banyusehat.files.wordpress.com/2014/05/proses2.jpg?w=300&amp;h=204" style="height:204px; width:300px" /></a></p>

      <p style="text-align:justify">&nbsp;</p>

      <hr />
      <p style="text-align:justify"><strong>MANFAAT</strong><br />
      Manfaat Minum &ldquo;Winair&rdquo; :</p>

      <ul>
	      <li style="text-align:justify">Antioksidan alami yang menetralkan radikal bebas dalam tubuh.</li>
	      <li style="text-align:justify">Membantu mempertahankan pH darah dan menurunkan tekanan darah.</li>
	      <li style="text-align:justify">Mengeluarkan dan menetralisir toxin/racun dari tubuh.</li>
	      <li style="text-align:justify">Memperbaiki metabolisme dan imunitas.</li>
	      <li style="text-align:justify">Membantu program penurunan berat badan.</li>
	      <li style="text-align:justify">Menurunkan kadar gula darah pada penderita diabetes.</li>
	      <li style="text-align:justify">Membantu mengurangi resiko penyakit : kanker &amp; tumor.</li>
	      <li style="text-align:justify">Membantu mengurangi resiko penyakit kardiovaskular (Jantung dan pembuluh darah).</li>
	      <li style="text-align:justify">Menghilangkan nyeri pada waktu haid dan masalah keputihan.</li>
	      <li style="text-align:justify">Membantu mengeluarkan dan mengikis batu ginjal</li>
	      <li style="text-align:justify">Memperbaiki gangguan pada ulkus gastro duodenum (lambung &ndash; usus 12 jari)</li>
	      <li style="text-align:justify">Mencegah dan membantu mengatasi masalah : asam urat, asma, alergi, ambeien, demam berdarah, maag, migrain, reumatik, sakit kepala dan susah buang air besar.</li>
      </ul>

      <p style="text-align:justify"><strong>Aturan Minum :</strong></p>

      <ul>
	      <li style="text-align:justify">Pencegahan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 1 sampai botol 2 per hari</li>
	      <li style="text-align:justify">Proses Pengobatan : Minum sebanyak mungkin, minimal 5 botol per hari</li>
      </ul>

      <p style="text-align:justify">Tidak ada batasan volume air minum. Semakin banyak &ldquo;Winair&rdquo; diminum maka semakin cepat masalah teratasi. Kecuali untuk penderita gagal ginjal, silakan konsultasikan dengan dokter. Air minum &ldquo;Winair&rdquo; bukan obat. Winair dengan sifat-sifatnya membantu tubuh agar tubuh itu sendiri yang mengatasi masalahnya.</p>

      <hr />
      <p style="text-align:justify">Berikut adalah pengalaman para penderita yang sudah mencoba air ini serta saran dan anjuran minumnya :</p>

      <ul>
	      <li style="text-align:justify"><strong>Batu ginjal</strong>&nbsp;</li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Sesuai dengan kesaksian beberapa penderita megalami kejadian sebagai berikut : beberapa jam setelah minum, air kencing menjadi keruh, disertai dengan keluarnya hasil kikisan batu berukuran pasir halus. Air kencing agak mengental bahkan bercampur darah ( hal ini disebabkan ada iritasi antara batu ginjal dengan salurn air kencing). Untuk batu ginjal yang berukuran masih kecil, umumnya batunya hancur, lalu keluar bersamaan dengan air kencing. Batu ginjal yang berukuran cukup besar, keluar sendiri batunya, namun memang lebih sakit waktu keluarnya karena iritasi batu ginjal dengan saluran kencing.</p>

      <ul>
	      <li style="text-align:justify"><strong>Asma</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">3 hari setelah minum, penderita mulai merasakan lebih baik. Setelah beberapa minggu minum, gejala asma tidak muncul lagi, bahkan sudah tidak minum obat lagi.</p>

      <ul>
	      <li style="text-align:justify"><strong>Tekanan Darah Tinggi</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Tekanan darah tinggi erat hubungannnya dengan kondisi pembuluh darah dan darah itu sendiri. Air minum Winair sifatnya basa sehingga dengan cangat cepat membersihkan pembuluh darah serta mengencerkan darah yang kental, sehingga tekanan darah yang tinggi akan turun hingga normal. Berdasarkan pengalaman di lapangan penedrita tekanan darah tinggi akan merasakan pening sebentar setelah minum air ini, tetapi setelah itu tekanan darahnya menurun dan normal.</p>

      <ul>
	      <li style="text-align:justify"><strong>Asam Urat</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Banyak penderita asam urat teratasi setelah minum &ldquo;Winair ini. Penderita asam urat ada yang baru mengalami ada juga yang sudah menahun. Untuk yang baru mengalami menderita asam urat, dengan minum air ini biasanya 2 sampai 3 hari langsung turun asam uratnya. Minum Winair sebanyak banyaknya, maka asam urat akan cepat turun. Penderita asam urat yang sudah menahun, perlu waktu agar masalah asam uratnya teratasi.</p>

      <ul>
	      <li style="text-align:justify"><strong>Nyeri Haid</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Untuk keluhan masalah nyeri pada waktu datang haid, banyak pengalaman di lapangan yang secara keseluruhan penderita merasa teratasi setelah minum air ini. Minum beberapa hari menjelang haid dan pada waktu haid, biasanya tidak akan merasakan nyeri haid lagi, selain itu haidnya lancar.</p>

      <ul>
	      <li style="text-align:justify"><strong>Ambeien</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Mungkin anda kurang yakin dan timbul pertanyaan apa hubungannya antara ambeien dengan minum air ini. Fakta di lapangan banyak penderita ambeien sembuh dengan cara minum air ini. Salah satu penderita yang sudah keluar darah pada waktu BAB, sekarang sudah sembuh bahkan bisa lagi makan makanan yang pedas. Bila ada yang ambeien, minum air ini sebanyak banyaknya, dalam beberapa hari bengkak di bagian dubur akan mengecil.</p>

      <ul>
	      <li style="text-align:justify"><strong>Nyeri kaki pada waktu sujud, tangan dan kaki kesemutan</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Masalah ini sudah banyak yang teratasi, biasanya ibu ibu yang sudah berumur di atas 40 tahun. Alhamdulillah setelah minum air ini beberapa hari masalah nyeri sendi, di lutut, pinggang, telapak kaki akibat reumatik dan asam urat akan hilang.</p>

      <ul>
	      <li style="text-align:justify"><strong>Maag</strong></li>
      </ul>

      <p style="margin-left:40px; text-align:justify">Air minum Winair sangat manjur untuk mengatasi masalah pencernaan terutama penyakit maag. Beberapa penderita merasakan mual dan bahkan muntah setelah minum air ini. Jangan panik, teruskan saja minum air ini karena dalam beberapa hari penyakit maag anda akan sembuh. Beberapa penderita yang sudah minum air ini benar benar merasakan manfaatnya. Sekarang sudah tidak minum obat maag dari dokter lagi, bahkan minum kopi, makan makanan yang pedas sudah bisa dinikmati kembali.</p>

      <p style="text-align:justify">&nbsp;</p>

      <hr />
      <p style="text-align:justify"><strong>KESAKSIAN</strong></p>

      <p style="text-align:justify"><br />
      <a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_1.jpg"><img alt="foto_testimony_1" class="alignleft size-full wp-image-61" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_1.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Vitalitas</strong><br />
      Perkenalkan saya H. Encep, saya tinggal di Rancaekek, Bandung. Awalnya saya mau minum air ini untuk menghilangkan capek dan pegal pegal (linu-linu). Anehnya Selain capek dan pegal linu hilang ada hal lain yang saya rasakan. Gairah saya bertambah dan waktu berhubungan dengan istri menjadi lama. Terima kasih kepada Pak Wahyu, Pak Yuyun yang sudah memberikan air yang luar biasa ini.</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_8.jpg"><img alt="foto_testimony_8" class="alignleft size-full wp-image-63" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_8.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Batu Ginjal (Batu ginjal keluar 3 jam setelah minum)</strong><br />
      Nama saya Erwin, Saya berangkat dari Cianjur ke Bandung dengan tujuan mau berobat ke Rumah Sakit Immanuel dikarenakan saya sudah tidak BAB selama 4 hari, buang air kecil sangat sedikit. Hasil USG memperlihatkan ada batu dalam ginjal saya. Pagi hari, sebelum ke rumah sakit, kakak saya memberi air terapi Win Air. 3 jam setelah minum saya langsung BAB, Kencing keruh bercampur sedikit darah diikuti dengan batu ginjal berukuran beras. Saya tidak jadi ke rumah sakit karena sudah merasa enak dan batu ginjal sudah keluar. Sekarang saya rutin minum air yang sangat luar biasa.</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_3.jpg"><img alt="foto_testimony_3" class="alignleft size-full wp-image-64" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_3.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Manusia Paramex</strong><br />
      Saya Dadi, kerja di Organda Garut. Saya dijuluki oleh teman teman sebagai manusia paramex, karena bertahun tahun saya setiap hari harus minum obat paramex, paling tidak 8 butir per hari. Kalau tidak minum obat yang saya rasakan adalah pusing kepala sebelah alias migrain. saya tidak bisa bekerja kalau tidak minum paramex. suatu hari Pak Wahyu datang bersama Pak Didin memberikan air putih dalam botol sebanyak 2 dus. Saya disuruh minum sekuatnya. Alhamdulillah setelah habis 2 dus migrain saya hilang. Saya sama sekali tidak pernah minum obat paramex lagi.</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_4.jpg"><img alt="foto_testimony_4" class="alignleft size-full wp-image-65" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_4.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Asam Urat</strong><br />
      Kaki kanan saya bengkak dan sakitnya bukan kepalang, Mantri di kampung saya bilang bahwa saya kena asam urat. Kebetulan ada rombongan dari Bandung mengadakan pengobatan masal di kampung saya. Saya pikir mau dikasih obat ternyata cuma dikasih air minum. saya dikasih 2 dus dan selama satu minggu saya diberi tugas untuk minum sebanyak banyaknya. Setiap hari ada perubahan sampai bengkak dan sakitnya hilang. Sekarang saya sudah bisa bekerja lagi, ke sawah dan dagang. ( Diceritakan oleh Suparman-Subang).</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><strong><a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_6.jpg"><img alt="foto_testimony_6" class="alignleft size-full wp-image-66" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_6.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a>Stroke</strong><br />
      Sehari hari saya bekerja sebagai supir bus dengan trayek Garut Jakarta. Sejak kena stroke saya tidak dapat bekerja lagi. Saya tidak bisa berjalan, tangan kiri tak bisa diangkat. Suatu hari saya kedatangan Pak Dadi dengan rombongan, menganjurkan untuk minum air terapi Win Air. Saya nurut saja karena ingin sembuh. Setelah satu minggu mulai ada perobahan, saya sudah bisa berdiri dan berjalan, tangan kiri saya sudah bisa diangkat.(Asep, Singaparna, Tasik)</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><a href="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_7.jpg"><img alt="foto_testimony_7" class="alignleft size-full wp-image-68" src="http://banyusehat.files.wordpress.com/2014/05/foto_testimony_7.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Stroke</strong><br />
      Karena penyakit darah tinggi, saya mengalami stroke. Dari bahu leher ke atas hampir tidak berfungsi. Bicara tidak normal, susah bicara dan rero (seperti cadel), kemudian susah menelan dan mengunyah pada waktu makan. Saya diperkenalkan air ini oleh saudara saya, Mulyono, yang berdomisili di Jl. Flamboyan,Perumnas Subang. Alhamdulillah, setelah habis 10 dus, keadaan berubah, saya sudah bisa bicara normal, makan enak dan pusing kepala sama sekali sudah hilang. Terima kasih ( Bapak Ruyadi, Bukit Permata RT 02, RW 01), Cimahi )</p>

      <p style="text-align:justify">&nbsp;</p>

      <p style="text-align:justify"><a href="http://banyusehat.files.wordpress.com/2014/05/foto-eman.jpg"><img alt="foto eman" class="alignleft size-full wp-image-67" src="http://banyusehat.files.wordpress.com/2014/05/foto-eman.jpg?w=584" style="float:left; height:100px; margin-left:10px; margin-right:10px; width:100px" /></a><strong>Pengobat Alternatif</strong><br />
      Assalamualaikum Wr.Wb., Perkenalkan saya Sulaeman, tinggal di Padasuka Bandung. Saya selain guru dari Seni Bela Diri Mustika juga sebagai pengobat alternatif baik untuk masalah medis maupun non medis. Saya sudah coba produk ini kepada istri saya yang awalnya bertahun tahun selalu minum obat maag dari dokter, sekarang tidak lagi.Kemudian saya juga sudah berikan air ini kepada banyak pasien, Alhamdulillah, masalah kesehatan mereka benar benar terbantu dengan minum air ini.</p>

      <p style="text-align:justify"><br />
      &nbsp;</p>

      <hr />
      <p style="text-align:justify"><strong>TANYA JAWAB</strong><br />
      Pertanyaan yang sering diajukan</p>

      <p style="text-align:justify"><strong>Bagaimana cara minum Winair ?</strong><br />
      Minum Winair saat perut dalam keadaan kosong. Pagi hari pada waktu bangun tidur langsung minum Winair sebelum aktifitas lainnya. Usahakan 1 botol (600 ml) habis : Pagi hari sesaat bangun tidur, siang hari sebelum makan, dan malam sebelum tidur. Pada waktu pengobatan usahakan jangan minum air mineral yang lain selain Winair, jangan minum air minum yang bersifat asam (cuka, minuman mengandung soda, kopi dll.)</p>

      <p style="text-align:justify"><strong>Berapa banyak minum Winair ?</strong><br />
      Kalau anda bisa minum banyak ya silakan. Idealnya minimal 3 liter per hari, tapi bila anda mau lebih silakan. Kalau dokter memberikan obat 3 x 2 kapsul per hari maka anda tidak boleh minum 3 x 4 kapsul per hari. Karena obat adalah racun (yang bila diminum dalam jumlah tertentu akan mengobat)i. Winair bukanlah obat jadi bila diminum lebih dari 3 liter ya tak ada masalah, malah lebih banyak lebih bagus. Pembuangan racun akan cepat, penyebaran oksigen dan nutrisi dari yang anda makan akan cepat diterima oleh seluruh sel dalam tubuh anda.</p>

      <p style="text-align:justify"><strong>Sampai kapan saya minum Winair ?</strong><br />
      Apakah setelah sembuh dari penyakit boleh saya berhenti minum Winair ?<br />
      Jangan beranggapan bahwa Winair adalah air obat. Obat HARUS berhenti diminum bila anda sudah sembuh. Bila anda sakit kepala, lalu sembuh setelah minum obat sakit kepala apakah terus minum obat sakit kepala ? Tentu tidak. Lain hal nya dengan Winair, bila sudah sembuh apakah harus berhenti minum air ?&nbsp;Winair bukan obat tetapi air minum yang dapat membantu tubuh untuk mengobati dirinya. Nah kalau tubuh kita tidak perlu bantuan lagi, ya tidak usah minum Winair. Masalahnya adalah, silakan evaluasi diri sendiri, apakah tubuh anda perlu bantuan untuk melawan penyakit yang datang setiap saat, setiap hari ?.</p>

      <p style="text-align:justify">Setiap hari kita perlu makan. Kita makan nasi, roti, mie, kentang dll sebagai karbohidratnya. Kita makan susu, daging, ikan, telur untuk proteinnya. Kita makan sayur dan buah buahan untuk vitamin dan mineralnya.Karbohidrat, protein bila dicerna oleh lambung selain menghasilkan energy pada waktu metabolisme juga menghasilkan sampah buangan berupa larutan yang bersifat ASAM. Sayur dan buah-buahan pada umumnya bila sudah dicerna akan menghasilkan larutan yang bersifat basa.</p>

      <p style="text-align:justify">Bila pola makan anda setiap hari sedikit sayur dan sedikit buah-buahan maka setiap hari sampah larutan yang bersifat ASAM akan masuk ke dalam darah anda lebih banyak. Apalagi bila anda jarang makan sayuran dan buah-buahan maka sampah asam setiap hari mengalir ke dalam darah anda. Akumulasi sampah asam setiap hari akan diendapkan dalam bentuk lemak jahat ( cholesterol), asam urat, batu ginjal. Oleh karenanya apabila pola makan anda selalu menghasilkan asam setiap hari maka sudah sepantasnya tubuh anda memerlukan bantuan setiap hari dengan minum Winair yang bersifat basa. Bila sudah sembuh dari penyakit maka jangan berhenti minum Winair. Mencegah lebih baik daripada mengobati.</p>

      <p style="text-align:justify"><strong>Winair bukan obat, mengapa air ini bisa mengobati penyakit ?</strong><br />
      Winair adalah air minum yang tidak mengandung nutrisi, tidak ada gula, tidak ada vitamin dan bukan minuman berenergy. Winair tidak mengandung nutrisi yang bisa memberikan anda energy, begitu juga Winair tidak mengandung obat apapun yang dapat menyembuhkan berbagai macam penyakit.</p>

      <p style="text-align:justify">Yang dilakukan oleh Winair adalah menetralkan keasaman tubuh, menciptakan sirkulasi darah yang lebih baik, menghilangkan sumbatan dalam pembuluh darah yang menghambat sirkulasi darah, mengangkut nutrisi dan oksigen lebih cepat dan lebih banyak, membuang racun dalam tubuh lebih cepat.</p>

      <p style="text-align:justify">Tubuh manusia sudah mempunyai sistem pertahanan sangat baik dari serangan virus, bakteri, radiasi gelombang elektro magnetik dll. Bahkan tubuh secara sistem dapat mengobati penyakit dengan sendirinya tanpa bantuan obat kimia, memperbaiki sel yang rusak, menumbuhkan sel sehat dan membuang racun dalam tubuh. Winair tidak dapat menyembuhkan penyakit apapun, tetapi dengan minum Winair, sirkulasi darah akan diperbaiki dengan cepat, darah akan menjadi encer walaupun tidak ada zat pengencer darah dalam Winair, sumbatan sumbatan dalam pembuluh darah akan dihilangkan.</p>

      <p style="text-align:justify">Sirkulasi darah yang baik mengakibatkan nutrisi dan oksigen lebih cepat beredar ke seluruh sel dalam tubuh, pembuangan racun tubuh juga cepat. Itulah fungsi Winair : membantu tubuh dapat bertahan dari serangan penyakit bahkan melawan penyakit oleh kekuatan tubuh sendiri.</p>
      ', 'Toko Online' =>
      '
      <p style="text-align:justify"><img alt="" src="/uploads/ckeditor/pictures/6/content_food-shopping-fff.jpg" style="float:left; height:300px; margin-left:10px; margin-right:10px; width:350px" />Toko Online saat ini sedang menjadi trend dalam dunia bisnis, kemudahan dan efesiensi waktu yang membuat para pebisnis beramai-ramai membuka toko online untuk pemasaran bisnis mereka, adanya peluang transaksi online yang sangat besar menjadikan toko online menjadi produk utama beliapaaja.com</p>

      <p style="text-align:justify">Di program afiliasi beliapaaja.com anda mendapatkan toko online dengan GRATIS anda cukup memasukan produk produk yang akan anda jual, yang menarik toko online yang anda miliki dapat di promosikan keseluruh dunia hanya mengupgrade untuk beriklan di web beliapaaja.com</p>

      <p style="text-align:justify">Untuk pertama kali toko online anda akan menyatu dengan domain program beliapaaja.com, contoh : www.beliapaaja.com/tokoairterapi . akan tetapi apabila anda ingin secara mandiri memiliki nama domain web toko online anda sendiri, anda cukup membeli domainnya lewat customer service di web beliapaaja.com</p>

      <p style="text-align:justify">Melalui toko online ini berharap siapapun di arahkan untuk memulai bisnisnya sendiri dan memiliki asset dari ide produknya sendiri, serta dapat menumbuhkan jiwa entrepreneurship para mitra program afiliasi beliapaaja.com</p>
    '}

    programs.each do |x,y|
      Program.create(title: x, description: y)
    end
  end
end
