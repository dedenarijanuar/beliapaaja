MlmCore::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  #devise_for :users
  #devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  #afiliasi frontend
#  get 'afiliasi' => 'afiliasi#home', as: 'afiliasi'
#  get 'afiliasi/profil/:slug' => 'afiliasi#page', as: 'afiliasi_page'
#  get 'afiliasi/program/:slug' => 'afiliasi#program', as: 'afiliasi_program'
#  get 'afiliasi/marketing/sistem_bisnis' => 'afiliasi#sistem_bisnis', as: 'afiliasi_sistem_bisnis'
#  get 'afiliasi/berita/list' => 'afiliasi#list_berita', as: 'afiliasi_list_berita'
#  get 'afiliasi/berita/:slug' => 'afiliasi#berita', as: 'afiliasi_berita'
#  get 'afiliasi/agenda/list' => 'afiliasi#list_agenda', as: 'afiliasi_list_agenda'
#  get 'afiliasi/agenda/:slug' => 'afiliasi#agenda', as: 'afiliasi_agenda'
#  get 'afiliasi/galleries' => 'afiliasi#galleries', as: 'afiliasi_galleries'
#  get 'afiliasi/faq' => 'afiliasi#faq', as: 'afiliasi_faq'
  
  #afiliasi backend
#  get 'afiliasi/profil_saya' => 'profiles#show', as: 'my_profile'
#  get 'afiliasi/ubah_profil' => 'profiles#edit', as: 'edit_profile'
#  post 'afiliasi/update_profile' => 'profiles#update', as: 'update_profile'
#  get 'afiliasi/ubah_pin' => 'profiles#change_pin', as: 'change_pin'
#  post 'afiliasi/update_pin' => 'profiles#update_pin', as: 'update_pin'
#  
#  get 'afiliasi/my_network' => 'networks#my_network', as: 'my_network'
#  get 'afiliasi/find_network' => 'networks#find_network', as: 'find_network'
#  get 'afiliasi/diagram_jaringan' => 'networks#diagram_network', as: 'diagram_network'
#  get 'afiliasi/pertumbuhan_jaringan' => 'networks#history_network', as: 'history_network'
#  get 'afiliasi/tambah_jaringan' => 'networks#add_network', as: 'add_network'
#  post 'afiliasi/create_network' => 'networks#create_network', as: 'create_network'
#  get 'afiliasi/afiliasiku' => 'networks#afiliasiku', as: 'afiliasiku'
#  
#  get 'afiliasi/bonus' => 'balances#bonus', as: 'bonus'
#  get 'afiliasi/transfer' => 'balances#transfer', as: 'transfer'
#  get 'afiliasi/history_saldo' => 'balances#history', as: 'history_balance'
#  get 'afiliasi/reward' => 'balances#reward', as: 'reward'
#  get 'afiliasi/support' => 'balances#support', as: 'support'
#  
#  get 'login' => 'sessions#login'
#  post 'process_login' => 'sessions#process_login'
#  delete 'logout' => 'sessions#logout'
#  
#  get 'afiliasi/registrasi' => 'registration#sign_up', as: 'sign_up'
#  post 'afiliasi/proses_registrasi' => 'registration#process_sign_up', as: 'process_sign_up'
  
#  get 'afiliasi/kotak_masuk' => 'messages#inbox', as: 'message_inbox'
#  get 'afiliasi/kotak_keluar' => 'messages#sentbox', as: 'message_sentbox'
#  get 'afiliasi/tulis_pesan' => 'messages#write', as: 'message_write'
#  post 'afiliasi/proses_tulis_pesan' => 'messages#process_write', as: 'message_process_write'
#  get 'afiliasi/balas_pesan' => 'messages#reply', as: 'message_reply'
#  post 'afiliasi/proses_balas_pesan' => 'messages#process_reply', as: 'message_process_reply'
#  post 'afiliasi/hapus/:type' => 'messages#delete', as: 'message_delete'
#  get 'afiliasi/pesan/:id' => 'messages#show', as: 'message_show'
#  
#  namespace :admin, :path => 'icdc' do
#    get 'antim' => 'home#serial', as: 'serial'
#    get 'antim_terkirim' => 'home#serial_terkirim', as: 'serial_terkirim'
#    get 'antim_tersedia' => 'home#serial_tersedia', as: 'serial_tersedia'
#    post 'antim_proses_dikirim' => 'home#serial_dikirim', as: 'serial_dikirim'
#    get 'user' => 'home#user', as: 'user'
#    get 'edit_user/:id' => 'home#edit_user', as: 'edit_user'
#    post 'update_user/:id' => 'home#update_user', as: 'update_user'
#    get 'transaction' => 'home#transaction', as: 'transaction'
#    post 'transfer_saldo' => 'home#transfer_saldo', as: 'transfer_saldo'
#    get 'paid_transaction' => 'home#paid_transaction', as: 'paid_transaction'
#  end
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
