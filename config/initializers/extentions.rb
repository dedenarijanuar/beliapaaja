module NumberFormat

  def display(params ={:unit => "Rp.", :delimiter => ".", :separator => ",", :precision => 0})
    raise "Only receive hash as parameters :unit, :delimiter, and :separator" unless params.is_a?Hash
    return "#{params[:unit]} 0#{params[:separator]}00" if [NilClass, String].include?self.class
    ActionController::Base.helpers.number_to_currency(self, :unit => "#{params[:unit]} ", :delimiter => params[:delimiter], :separator => params[:separator], :precision => params[:precision])
  end

end

module StringFormat
  def string_encrypt
    string = self.split(' ').map{|x| Base64.encode64(x) }.join('_')
    return Base64.encode64(string)
  end
  
  def string_decrypt
    Base64.decode64(self).split('_').map{|x| Base64.decode64(x)}.join(' ')
  end
end

module CustomDate

  def to_dmy
    self.strftime("%d/%m/%Y") rescue '-'
  end

  def to_dby
    self.strftime("%d-%B-%Y") rescue '-'
  end
end

module CustomDateTime

  def to_dmyhm
    self.strftime("%d-%m-%Y %H:%M") rescue '-'
  end

  def to_dmy
    self.strftime("%d-%m-%Y") rescue '-'
  end
  
  def to_hm
    self.strftime("%H:%M") rescue '-'
  end

end


String.class_eval do
  include NumberFormat
  include StringFormat
end

Integer.class_eval do
  include NumberFormat
end

Date.class_eval do
  include CustomDate
end

DateTime.class_eval do
  include CustomDateTime
end

Time.class_eval do
  include CustomDateTime
end

