#root = "/home/beliapaaja"
#working_directory root
#pid "#{root}/tmp/pids/unicorn.pid"
#stderr_path "#{root}/log/unicorn.log"
#stdout_path "#{root}/log/unicorn.log"

#listen "/tmp/unicorn.blog.sock"
#worker_processes 2
#timeout 30

# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/home/beliapaaja"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/home/beliapaaja/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/home/beliapaaja/log/unicorn.log"
stdout_path "/home/beliapaaja/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.beliapaaja.sock"
listen "/tmp/unicorn.myapp.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
